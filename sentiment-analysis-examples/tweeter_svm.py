import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import time
from sklearn import svm
from sklearn.metrics import classification_report

# train data
trainData = pd.read_csv("data/tweet_train.csv")

# test data
testData = pd.read_csv("data/tweet_test.csv")

print(trainData.sample(frac=1).head(5))

# Create feature vectors

vectorizer = TfidfVectorizer(
    min_df=5,
    max_df=0.8,
    sublinear_tf=True,
    use_idf=True)

train_vectors = vectorizer.fit_transform(trainData['Tweet'])
test_vectors = vectorizer.transform(testData['Tweet'])


# perform classification with kernel=Linear

classifier_linear = svm.SVC(kernel="linear")
t0 = time.time()
classifier_linear.fit(train_vectors, trainData['Category'])
t1 = time.time()
prediction_linear = classifier_linear.predict(test_vectors)
t2 = time.time()
time_linear_train = t1-t0
time_linear_predict = t2-t1

# results
print("Training time: %fs; Prediction time: %fs" % (time_linear_train, time_linear_predict))

# report = classification_report(testData['Label'], prediction_linear, output_dict=True)

# print('positive: ', report['pos'])
# print('negative: ', report['neg'])



review = """This item is really bad for me"""
review_vector = vectorizer.transform([review])
print(classifier_linear.predict(review_vector))