import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.naive_bayes import GaussianNB


df_train = pd.read_csv('data/ExtractedTweetsRefined.csv')
df_test = pd.read_csv('data/tweet_test.csv')

le = preprocessing.LabelEncoder()
Label_Encoded_train = le.fit_transform(df_train['Tweet'])

print(Label_Encoded_train)
Feature_Encoded_train = le.fit_transform(df_train['Party'])
#Feature_Encoded_train = np.array(Feature_Encoded_train).reshape((-1, 1))

Feature_Encoded_test = le.fit_transform(df_train['Tweet'])
Feature_Encoded_test = np.array(Feature_Encoded_test).reshape((-1, 1))

print(Feature_Encoded_train.shape)

# model = GaussianNB()
# model.fit(Feature_Encoded_train, Label_Encoded_train)

# predicted= model.predict(Feature_Encoded_test)

# print(predicted)

