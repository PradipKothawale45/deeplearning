import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from sklearn.metrics import classification_report
from sklearn import metrics
from sklearn.model_selection import train_test_split
from bs4 import BeautifulSoup
from nltk.tokenize import WordPunctTokenizer
import warnings
import re
import time
import pickle
import sys


infile = 'data/ExtractedTweetsRefined.csv'
model_name = 'finalized_model2.sav'

class Classifier:
    
    def __init__(self):
        self._vectorizer = self.vectorize()
        self.X_train, self.X_test, self.y_train, self.y_test = self.load_data()
    
    def vectorize(self):
        # vectorization
        vectorizer = TfidfVectorizer(
            min_df=5,
            max_df=0.8,
            sublinear_tf=True,
            use_idf=True)
        return vectorizer

    def tweet_cleaner(self, text):
        warnings.filterwarnings("ignore", category=UserWarning, module='bs4')
        tok = WordPunctTokenizer()
        pat1 = r'@[A-Za-z0-9]+'
        pat2 = r'https?://[A-Za-z0-9./]+'
        combined_pat = r'|'.join((pat1, pat2))
        soup = BeautifulSoup(text, 'lxml')
        souped = soup.get_text()
        stripped = re.sub(combined_pat, '', souped)
        try:
            clean = stripped.decode("utf-8-sig").replace(u"\ufffd", "?")
        except:
            clean = stripped
        letters_only = re.sub("[^a-zA-Z]", " ", clean)
        lower_case = letters_only.lower()
        words = tok.tokenize(lower_case)
        return (" ".join(words)).strip()

    def load_data(self):
        trainData = pd.read_csv(infile)
        # drop the unnecessary columns
        trainData.drop(['Handle'], axis=1, inplace=True)
        for i in range(len(trainData)):
            trainData['Tweet'].values[i] = self.tweet_cleaner(trainData['Tweet'].values[i])
        train_vectors = self._vectorizer.fit_transform(trainData['Tweet'])
        X_train, X_test, y_train, y_test = train_test_split(train_vectors, trainData['Party'], test_size=0.3,random_state=109)
        return X_train, X_test, y_train, y_test
    
    def train_model(self):
        classifier_linear = svm.SVC(kernel="linear")
        t0 = time.time()
        classifier_linear.fit(self.X_train, self.y_train)
        t1 = time.time()
        prediction_linear = classifier_linear.predict(self.X_test)
        t2 = time.time()
        time_linear_train = t1-t0
        time_linear_predict = t2-t1
        print("Accuracy:",metrics.accuracy_score(self.y_test, prediction_linear))

        print("Classification Report:\n")
        print(classification_report(self.y_test, prediction_linear))

        pickle.dump(classifier_linear, open(model_name, 'wb'))
        print("Training time: %fs; Prediction time: %fs" % (time_linear_train, time_linear_predict))
        print("Training has been completed.")
        return classifier_linear

    def prediction(self, sentence):
        classifier_linear = pickle.load(open('finalized_model2.sav', 'rb'))
        review_vector = self._vectorizer.transform([sentence])
        return classifier_linear.predict(review_vector)

if __name__ == '__main__':
    cl = Classifier()
    if sys.argv[1] == 'train':
        cl.train_model()
    
    if sys.argv[1] == 'predict':
        while True:
            sentence = input("Enter the Tweet:")
            sentence = cl.tweet_cleaner(sentence)
            result = cl.prediction(sentence)
            print(result)