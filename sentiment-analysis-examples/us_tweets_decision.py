import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn import metrics
from sklearn.model_selection import train_test_split
import time
import pickle
import sys


infile = 'data/ExtractedTweetsRefined.csv'
model_name = 'finalized_model_tree.sav'

class Classifier:
    
    def __init__(self):
        self._vectorizer = self.vectorize()
        self.X_train, self.X_test, self.y_train, self.y_test = self.load_data()
    
    def vectorize(self):
        # vectorization
        vectorizer = TfidfVectorizer(
            min_df=5,
            max_df=0.8,
            sublinear_tf=True,
            use_idf=True)
        return vectorizer
    
    def tweet_cleaner(self, text):
        warnings.filterwarnings("ignore", category=UserWarning, module='bs4')
        tok = WordPunctTokenizer()
        pat1 = r'@[A-Za-z0-9]+'
        pat2 = r'https?://[A-Za-z0-9./]+'
        combined_pat = r'|'.join((pat1, pat2))
        soup = BeautifulSoup(text, 'lxml')
        souped = soup.get_text()
        stripped = re.sub(combined_pat, '', souped)
        try:
            clean = stripped.decode("utf-8-sig").replace(u"\ufffd", "?")
        except:
            clean = stripped
        letters_only = re.sub("[^a-zA-Z]", " ", clean)
        lower_case = letters_only.lower()
        words = tok.tokenize(lower_case)
        return (" ".join(words)).strip()
    
    def load_data(self):
        trainData = pd.read_csv(infile)
        # drop the unnecessary columns
        trainData.drop(['Handle'], axis=1, inplace=True)
        for i in range(len(trainData)):
            trainData['Tweet'].values[i] = self.tweet_cleaner(trainData['Tweet'].values[i])
        train_vectors = self._vectorizer.fit_transform(trainData['Tweet'])
        train_vectors_y = self._vectorizer.fit_transform(trainData['Party'])
        X_train, X_test, y_train, y_test = train_test_split(train_vectors, train_vectors_y, test_size=0.3,random_state=109)
        return X_train, X_test, y_train, y_test
    
    def train_model(self):
        model = RandomForestClassifier(n_estimators=100)
        t0 = time.time()
        model.fit(self.X_train.todense(), self.y_train.todense())
        t1 = time.time()
        prediction_linear = model.predict(self.X_test.todense())
        t2 = time.time()
        time_linear_train = t1-t0
        time_linear_predict = t2-t1
        print("Accuracy:",metrics.accuracy_score(self.y_test, prediction_linear))

        print("Classification Report:\n")
        print(classification_report(self.y_test, prediction_linear))

        pickle.dump(model, open(model_name, 'wb'))
        print("Training time: %fs; Prediction time: %fs" % (time_linear_train, time_linear_predict))
        print("Training has been completed.")
        return model

    def prediction(self, sentence):
        classifier_linear = pickle.load(open('finalized_model_tree.sav', 'rb'))
        review_vector = self._vectorizer.transform([sentence])
        return classifier_linear.predict(review_vector.todense())

if __name__ == '__main__':
    cl = Classifier()
    if sys.argv[1] == 'train':
        cl.train_model()
    
    if sys.argv[1] == 'predict':
        sentence = input("Enter the Tweet:")
        sentence = cl.tweet_cleaner(sentence)
        result = cl.prediction(sentence)
        print(result)